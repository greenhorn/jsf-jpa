
import com.mycompany.mavenproject1.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author greenhorn
 */
public class Manager {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("florist");
    EntityManager em = emf.createEntityManager();
    EntityTransaction entityTransaction = em.getTransaction();

    public List<User> getAllUsers() {
        TypedQuery<User> query
                = em.createNamedQuery("User.findAll", User.class);
        List<User> results = query.getResultList();
        return results;
    }
}
