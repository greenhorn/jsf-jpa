
import com.mycompany.mavenproject1.User;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author greenhorn
 */
@SessionScoped
@ManagedBean
public class Bean {
    private Manager mgr;

    public Bean() {
        mgr = new Manager();
    }

    public String getUsers() {
        List<User> users = mgr.getAllUsers();

        String userNames = "";
        for (User user : users) {
            userNames += user.getId()+" ";
        }

        return userNames;
    }
}
